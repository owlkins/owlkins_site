"""owlkins_site URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from django_distill import distill_path
from django.shortcuts import render
import inspect
from django.conf import settings


def return_distill_path(template, root=False, other=False):
    calling_module = inspect.stack()[1][1].replace('/urls.py', '').split("/")[-1] + '/'
    full_name = '{name}{template}'.format(name='' if root else calling_module, template=template)
    return distill_path(template,
                        lambda request: render(request, '{}.html'.format(other or full_name), {
                            'template': template,
                            'full_name': full_name,
                            'other': other,
                        }),
                        name=full_name,
                        distill_func=lambda: None,
                        distill_file='{}/index.html'.format(full_name))


urlpatterns = [
    distill_path('',
                 lambda request: render(request, 'index.html', {}),
                 name='index',
                 distill_func=lambda: None,
                 distill_file='index.html'),
    return_distill_path('404', root=True),
] + [path('{}/'.format(each), include('{}.urls'.format(each)))
     for each in settings.TEMPLATE_APPS]
