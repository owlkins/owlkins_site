from owlkins_site.urls import return_distill_path

urlpatterns = [
    return_distill_path('registry'),
    return_distill_path('photos'),
    return_distill_path('data'),
    return_distill_path('overview'),
]