const colors = require('tailwindcss/colors')
const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
    content: [
        "../documentation/**/*.{html,js}",
        "../features/**/*.{html,js}",
        "../info/**/*.{html,js}",
        "../legal/**/*.{html,js}",
        "../support/**/*.{html,js}",
        "../templates/**/*.{html,js}",
    ],
    theme: {
        extend: {
            colors: {
                maincolor: colors.amber,
            },
            fontFamily: {
                sans: ['Inter var', ...defaultTheme.fontFamily.sans],
              },
        },
    },
    plugins: [
        require('@tailwindcss/forms'),
        require('@tailwindcss/aspect-ratio'),
        require('@tailwindcss/typography'),
    ],
}
