from owlkins_site.urls import return_distill_path

urlpatterns = [
    return_distill_path('overview'),
    return_distill_path('install'),
    return_distill_path('architecture'),
    return_distill_path('changelog'),
    return_distill_path('aws-ses-usecase'),
    return_distill_path('backup-script'),
]