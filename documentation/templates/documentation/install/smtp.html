{% load code_tags %}
{% load documentation_tags %}
{% documentation_section with title="Setup Amazon SES (or other SMTP)" %}
<p>Email is a lot more complex than people think, and can be quite a rabbit hole to go down.
    Very likely you need an SMTP relay to send messages if you want the emails you send to have a higher chance of
    skipping a user's spam filter. You absolutely need an SMTP relay if you decide to self-host on a machine at your
    house behind a residential IP address as any modern spam list should and will include a blanket block (not necessarily
    even a spam label) on email originating from residential IP addresses. In terms of inbound mail, most of the major
    providers like XFINITY will block all traffic on the ports that mail servers send and receive mail on so if you
    were to go so far as to setting up a mailserver "at home" for inbound mail on your domain, you'd have to setup some
    <b>iptables</b> MASQUERADE rule on some external server before sending to your IP address on some non-standard port.
    Fortunately, for the scope of Owlkins, inbound mail is not needed at all so you can focus solely on <i>sending</i>
    high quality mail.
</p>
<p>You can use any SMTP relay such as <a target="_blank" href="https://www.mailgun.com/">Mailgun</a>,
<a target="_blank" href="https://sendgrid.com/">Sendgrid</a>, or <a target="_blank" href="https://www.mailjet.com/">Mailjet</a>, but we will discuss
    <a target="_blank" href="https://aws.amazon.com/ses/">AWS Simple Email Service below</a> as I find it nice that you can
    consolidate both Email and distribution of photos and videos using
    <a target="_blank" href="https://aws.amazon.com/cloudfront/">AWS's CloudFront</a> Content Distribution Network (CDN)
    into one bill. The downside of AWS SES is they may deny your use case if you are not already approved to send
    email on your account, especially if you just made an account (see more below on <b>Step 18</b>).</p>
<p>The goal of any SMTP relay is to populate the following values:</p>
<p><i>in <code>config.py</code>, populate</i>
    <ul>
    <li><code>EMAIL_HOST</code> with the smtp relay. For the above example with <b>us-east-2</b>, the value would be
    <code>email-smtp.us-east-2.amazonaws.com</code></li>
    <li><code>EMAIL_DOMAIN</code> with the domain that you add to SES as a "<b>Verified Identity</b>". You have to prove
    that you have the right to send mail from the domain by adding DNS records that SES will generate for you.</li>
</ul>
</p>
<p>To get started with SES, sign into your AWS account and navigate to the dashboard for the region you wish to send
email from. For example, here is the
<a target="_blank" href="https://us-east-2.console.aws.amazon.com/ses/home?region=us-east-2#/verified-identities">dashboard for us-east-2</a>
    Verified Identites page. Here you setup the domain:
<ol>
    <li>Click <b>Create Identity</b></li>
    <li>Click <b>Domain</b> for the Identity type</li>
    <li>Type your domain into the text field</li>
    <li>Leave "Assign a default configuration set" unchecked</li>
    <li>Check "Use a custom MAIL FROM domain"</li>
    <li>Fill in the "mail from" domain (a subdomain), typically <code>mail</code></li>
    <li>Leave <b>Behavior on MX failure</b> as <b>Use default MAIL FROM domain</b></li>
    <li>Under the next section, <b>Verifying your domain</b>, click <b>Advanced DKIM settings</b> to reveal more options</li>
    <li>Select <b>Easy DKIM</b> as the <b>Identity Type</b></li>
    <li>Select <b>RSA_2048_BIT</b> as the <b>DKIM signing key length</b></li>
    <li>If you don't manage your domain on AWS, through Route53, then <b>uncheck</b> the box "Enabled" for
        <b>Publish DNS records to Route53</b></li>
    <li>Click <b>Create Identity</b></li>
    <li>Now the AWS Dashboard will redirect to a new view showing your new identity. Next you will have to modify
    your DNS records with several values that were just created for your verified domain.
        <ol>
            <li>There are three <b>CNAME</b> values that you will have to add to your domain. If you are using
            a site like Google Domains for your DNS settings, make sure you appropriately copy the key over, as a
            direct COPY + PASTE will result in copying the value from AWS's dashboard like
                <code>1234._domainkey.yourdomain.com</code> which when pasted directly into Google's DNS UI will
                result in a setting like <code>1234._domainkey.yourdomain.com.yourdomain.com</code> which is not correct.</li>
            <li>There is one <b>MX</b> value you will have to add for the <b>mail from</b> domain you selected in
            the earlier step, where if you follow the above with <code>mail</code> and <b>us-east-2</b>, then add
            a <b>MX</b> record for subdomain <b>mail</b> with value <code>10 feedback-smtp.us-east-2.amazonses.com</code></li>
            <li>There is one <b>TXT</b> value you will have to set for the <b>mail from</b> domain which is typically
            something like <code>"v=spf1 include:amazonses.com ~all"</code> (double quotes included).</li>
        </ol>
    </li>
    <li>Once your domain is verified, it is now time to get your credentials. Click the <b>Account dashboard</b> button
    on the left navigation bar. Scroll down to the <b>Simple Mail Transfer Protocol (SMTP) settings</b> section, where
    there is a button <b>Create SMTP credentials</b></li>
    <li>A new window will load now in the <b>IAM</b> dashboard view where you can set a username or accept the default
    which is usually something like <b>ses-smtp-user.20220130-210943</b>. This username is for you to easily identify
    the credentials in your IAM settings and does not relate to any external usage.</li>
    <li>Click the <b>Create</b> button in the bottom right, and reveal the credentials in the next view by
    clicking <b>Show User SMTP Security Credentials</b> and/or selecting the <b>Download Credentials</b> button in the
    bottom right. <i>Note:</i> After you navigate away from this page the credentials will no longer be retrievable.</li>
    <li>These two values should now be saved in your <code>secrets.py</code> file:
        <ul>
            <li><b>SMTP Username:</b> should be stored in the <code>EMAIL_HOST_USER</code> variable</li>
            <li><b>SMTP Password:</b> should be stored in the <code>EMAIL_HOST_PASSWORD</code> variable</li>
        </ul>
    </li>
    <li><b>Permission:</b> Now you are all set up to send email, except, you may not be authorized to send!
    Go back to the <a target="_blank" href="https://us-east-2.console.aws.amazon.com/ses/home?region=us-east-2#/account">Acount Dashboard</a>
    and see if it says <b>Your Amazon SES account is in the sandbox</b> for your chosen region. In this section there
    will be a button <b>Request production access</b> which will then take you to a new form.
    <ol>
        <li><b>Mail type:</b> Transactional (your Owlkins server will send emails allowing users to set their password and daily
        email digests announcing new photos and videos) </li>
        <li><b>Website URL:</b> You can either use your Owlkins distribution URL, or if needed you can try to send
            <a href="{% url 'documentation/aws-ses-usecase' %}" target="_blank">this link</a> to an explainer on our
            site about Owlkins email activity.</li>
        <li><b>Use case description:</b> AWS will need to ensure that you are only sending emails that are high quality
        and won't ruin the reputation of Amazon's servers. Try to write a personal message and note these key points
            quoted from a denial email:
            <p><i>tell us how often you send email, how you maintain your recipient lists, and how you manage bounces,
                complaints, and unsubscribe requests. It is also helpful to provide examples of the email you plan to
                send so we can ensure that you are sending high-quality content.</i></p>
        </li>
    </ol></li>
    <li>Check the <b>Acknowledge</b>s checkbox and then Click <b>Submit request</b>
    </li>
</ol>
</p>
<p>Now you are all finished with setting up email! If you find yourself repeatedly rejected by Amazon, try to use
one of the other providers, where the steps to authorize a domain via DNS and getting credentials will be
pretty similar.</p>
{% end_documentation_section %}