{% load code_tags %}
{% load documentation_tags %}
{% documentation_section with title="Setup AWS S3 and Cloudfront" %}
<p>As discussed in the <a target="_blank" href="{% url 'documentation/architecture' %}">Architecture</a> page, one of the biggest
impacts you can have on your user's performance is to serve your photos and videos from a Content Distribution Network
(CDN). While completely necessary if hosting Owlkins behind a residential connection, it is still important even
if you purchase the biggest, fastest VM with the most I/O that you can find from some provider. Why? You just can't
beat a CDN at its own game: getting your content to your users as fast and reliably as possible.</p>
<p>We will also discuss Amazon S3 here, which as you may have guessed is the main driver of the "S3-compatible" software
ecosystem that you can now find at many competing vendors. Some S3-compatible solutions also combine the "S3" and "CDN"
aspects into one product like <a target="_blank" href="https://www.digitalocean.com/products/spaces">DigitalOcean Spaces</a>, however
I think the <a target="_blank" href="https://aws.amazon.com/s3/">AWS S3</a> + <a target="_blank" href="https://aws.amazon.com/cloudfront/">AWS CloudFront</a>
combination is especially important for Owlkins users who want to maintain privacy while also reaping the benefits
of a CDN. With S3, you can lock down all content such that only your CloudFront distribution can access it, and then
on CloudFront you can setup
<a target="_blank" href="https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/private-content-trusted-signers.html">trusted
signers</a> to enforce access to your photos and videos. This feature does take some extra time to setup, but we will
go into the steps below.</p>
<h4>Create the S3 bucket</h4>
<p>First we must create the S3 bucket that you will store your photos and videos in. From your AWS console:
<ol>
    <li>Navigate to the <a target="_blank" href="https://s3.console.aws.amazon.com/s3/home">S3 dashboard</a></li>
    <li>Click the <b>Create bucket</b> button to create a new bucket</li>
    <li>Give the bucket a name in the <b>Bucket name</b> section.
        <ul><li>Note that this should not be visible to your users once you finish setting up CloudFront below, but at
            some level the name will be publicly visible so name appropriately</li>
        </ul>
    </li>
    <li>Choose the AWS region or leave it default. The region will have a minor impact in uploading files, choose
    a region geographically closest to your server running Owlkins.
    </li>
    <li>Object Ownership you can leave with the default <b>ACLs disabled (recommended)</b></li>
    <li><b>Block Public Access settings for this bucket</b> you will want to leave set as the default <b>Block
        <i>all</i> public access</b>, as users will solely receive content through CloudFront.</li>
    <li><b>Bucket Versioning</b> you can leave <b>disabled</b>, but this is not a hard requirement and do as you see
        fit.
        <ul>
            <li>There is a sample <a target="_blank" href="{% url 'documentation/backup-script' %}">backup script</a> you can follow
            if you want to keep all files uploaded to S3 in sync with local storage for safekeeping.</li>
        </ul>
    </li>
    <li><b>Default encryption</b> you can generally as the default (<b>disabled</b>), but if you know what you're
        doing you can optionally set this up.</li>
    <li><b>Advanced Settings</b> you can leave with the defaults</li>
    <li>Now, click <b>Create bucket</b> to proceed</li>
    <li><b><u>Important:</u></b> now, take your bucket name and store this in the <code>AWS_STORAGE_BUCKET_NAME</code>
    variable in your <code>config.py</code> file.</li>

</ol>
</p>
<h4>CloudFront</h4>
<p>
Next we must create the CloudFront distribution that will transmit the photos and videos from S3. From your AWS console:
</p>
<p>
<h4>Creating a public key pair</h4>
<p>You will need to upload a public key
which you will use later. To make the below steps without extra friction, let's make the key first.
<ol>
    <li>Go to the <a target="_blank" href="https://console.aws.amazon.com/cloudfront/v3/home#/publickey">public key</a> part of your
    CloudFront dashboard
    </li>
    <li>Click <b>Create public key</b></li>
    <li>Type in any name that you want. This name will only be displayed internally in your AWS dashboard</li>
    <li>Enter a description, if you want.</li>
    <li>The <b>Key</b> field is the most important part of this form and the below steps are borrowed from <a target="_blank" href="https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/private-content-trusted-signers.html?icmpid=docs_cf_help_panel#private-content-creating-cloudfront-key-pairs">this link</a>:
        <ol>
            <li>On your local machine, with <code>openssl</code> installed, navigate to a folder that you want to store
            your keys in. Don't store your keys in a public or easily accessed location.</li>
            <li>Invoke the following to create your <b>private key</b> {% code_snippet %}openssl genrsa -out private_key.pem 2048{% endcode_snippet %}</li>
            <li>Next invoke the following to create your<b>public key</b>
                {% code_snippet %}openssl rsa -pubout -in private_key.pem -out public_key.pem{% endcode_snippet %} </li>
            <li>Take the contents of the <b>public key</b> in its entirety and paste it into the <b>Key</b> field back
            on your CloudFront dashboard</li>
            <li>Take the contents of the <b>private key</b> in its entirety and store it in the
                <code>CLOUDFRONT_PRIVATE_KEY</code> variable in your <code>secrets.py</code> file. Note that you will
                have to finangle the content a bit if you "paste" it in, as newline characters will have to be
            replaced by <code>\n</code> throughout the file. Alternatively, if you want to read in the contents from your
            <b>private_key.pem</b> file, you could do something like this: <pre><code>'CLOUDFRONT_PRIVATE_KEY': open('/absolute/path/to/your/private_key.pem').read()</code></pre></li>
        </ol>
    </li>
    <li>Click <b>Create public key</b> to save your key</li>
    <li>Now you will be taken back to your <a target="_blank" href="https://console.aws.amazon.com/cloudfront/v3/home#/publickey">public key</a>
    dashboard where you will see your new key and a value in the <b>ID</b> column
    </li>
    <li>Copy the value in the <b>ID</b> column and store this value in the <code>CLOUDFRONT_KEYPAIR_ID</code> field in
    your <code>secrets.py</code> file.</li>
</ol>
</p>
<h4>Requesting a certificate</h4>
<p>When you make your distribution, you will reach a point that you can set up an <b>Alternative domain name (CNAME)</b>,
    which you will have to do for you to use the keypair ID you just created above. One prerequisite to this step is
    to request a certificate for this domain. The domain name you choose should be a subdomain of your main domain.
    For example if you purchased <code>example.com</code> above and plan to host Owlkins at this domain, you could
    now choose <code>cdn.example.com</code> as the subdomain to host your CloudFront distribution at. For this step,
    your DNS page open for your domain so that you can quickly copy some CNAME values into your DNS records.
<ol>
    <li>Navigate to the <a target="_blank" href="https://console.aws.amazon.com/acm/home?region=us-east-1#/certificates/request">
    certificate request</a> dialog.</li>
    <li>Choose <b>Request a public certificate</b> and click <b>next</b></li>
    <li>Enter your <b>Fully qualified domain name</b>, which following the above example, would be
    <code>cdn.example.com</code></li>
    <li>For <b>Select validation method</b> leave <b>DNS validation - recommended</b> selected</li>
    <li>Click <b>Request</b></li>
    <li>Now you should be taken to your <a target="_blank" href="https://console.aws.amazon.com/acm/home?region=us-east-1#/certificates/list">
        certificate dashboard</a> where you see a message like
        <i>Successfully requested certificate with ID XXXXX-XXXX-XX</i> at the top, but no certificate in your dashboard!
        <ul><li>Don't panic, typically it takes a few seconds for the certificate to be created. Try refreshing your dashboard.</li></ul>
    </li>
    <li>Once your certificate populates the list on the dashboard, click it to go to the more detailed view</li>
    <li>In the <b>Domains</b> section you should see a <b>CNAME</b> value that you will have to configure
    at your domain's DNS settings. Follow the steps as you did above for SMTP setup to add the CNAME to your domain.</li>
    <li>Wait for the status to switch to <b>Issued</b> with a green checkmark before proceeding.</li>
</ol>
</p>
<h4>Creating the distribution</h4>
<p>Now that you have a keypair ID to sign content and a certificate for your CDN subdomain,
    we can move on to creating the distribution.
<ol>
    <li>Navigate now to the <a target="_blank" href="https://console.aws.amazon.com/cloudfront/v3/home">CloudFront dashboard</a></li>
    <li>Click the <b>Create distribution</b> button</li>
    <li>For the <b>Origin domain</b> select the S3 bucket you created above from the dropdown after clicking on the field.</li>
    <li>You can skip the <b>Origin path</b> and <b>Name</b> fields.</li>
    <li><b>S3 bucket access</b>
        <ol>
            <li>change to <b>"Yes use OAI (bucket can restrict access to only CloudFront)"</b></li>
            <li>Click <b>Create new OAI</b> to create a new Origin access identity</li>
            <li>After creating the OAI, select it in the dropdown for <b>Origin access identity</b></li>
            <li>Change the <b>Bucket policy</b> to <b>Yes, update the bucket policy</b>, so that AWS will handle the
            configuration needed for your S3 bucket.</li>
        </ol>
    </li>
    <li><b>Add custom header</b>: this can be left alone, skip it</li>
    <li><b>Enable Origin Shield</b>, you can leave this as the default <b>No</b></li>
    <li><b>Additional settings</b> you can skip</li>
    <li>Now under <b>Default cache behavior</b> you can leave <b>Path pattern</b> as <b>Default (*)</b></li>
    <li><b>Compress objects automatically</b> you can leave as <b>Yes</b></li>
    <li><b>Viewer protocol policy</b> you should change to <b>Redirect HTTP to HTTPS</b> though you should never have
    anyone accessing a link via HTTP anyway from Owlkins</li>
    <li><b>Allowed HTTP methods</b> leave as <b>GET, HEAD</b></li>
    <li><b>Restrict Viewer Access</b>: this is the portion that enforces access permissions to keep your content private.
        <ol>
            <li>Change to <b>Yes</b></li>
            <li><b>Trusted authorization type</b> keep as <b>Trusted key groups (recommended)</b>, and now you will have
            to make the key group by clicking the <b>Create key group</b> link and following it into a new tab.
                <ol>
                    <li>Enter anything you like in the <b>Name</b>, this should only be displayed on your dashboard</li>
                    <li>Enter anything you like for the <b>Description</b></li>
                    <li>For the <b>Public Keys</b> dropdown, select the keypair <b>ID</b> you created earlier.</li>
                    <li>Click <b>Create key group</b></li>
                </ol>
            </li>
            <li>Now with the <b>key group</b> created, you can go back to the tab for creating your distribution, and
                after clicking the refresh button next to the dropdown for <b>Add key groups</b> you should
            be able to select the key group you just created above.</li>
        </ol>
    </li>
    <li><b>Cache key and origin requests</b> you can leave as <b>Cache policy and origin request policy (recommended)</b></li>
    <li><b>Function associations</b> you can skip</li>
    <li>Under <b>Settings</b> the default <b>Price class</b> should usually be changed to <b>Use only North America and
        Europe</b> if this describes the bulk of your users. If you have many users in Asia, consider leaving the default
    <b>Use all edge locations (best performance)</b> though note that this does come with an increased cost.</li>
    <li><b>Alternate domain name (CNAME)</b> though this is listed as (optional) it is not optional for Owlkins
    users who want to use the signed cookies and urls generated by the keypair ID we created above.
        <ol>
            <li>Enter a domain name that is a subdomain of your main domain, for example if you purchased <code>example.com</code>
            above and plan to host Owlkins at this domain, and you followed the steps above for
                <b>requesting a certificate</b> your value here <code>cdn.example.com</code>.</li>
            <li>Select the certificate you made above.</li>
            <li>Leave <b>Security Policy</b> as the TLS version that is <b>(recommended)</b></li>
        </ol>
    </li>
    <li><b>Supported HTTP versions</b> leave <b>HTTP/2</b> checked</li>
    <li><b>Default root object</b> leave blank and ignore</li>
    <li><b>Standard logging</b> leave <b>off</b></li>
    <li><b>IPv6</b> leave <b>on</b></li>
    <li>Fill in <b>Description</b> with anything you like</li>
    <li>Click <b>Create distribution</b> to finalize these settings, where you will be taken back to your
    <a target="_blank" href="https://console.aws.amazon.com/cloudfront/v3/home#/distributions">Cloudfront Distributions</a> dashboard.</li>
    <li>Now the distribution is spinning up and you can take the <b>Alternative Domain Name</b> you used in configuring
    your CloudFront distribution and save this as the <code>AWS_CLOUDFRONT_DOMAIN</code> variable in your
    <code>config.py</code> file. This is not in your secrets file because this will be a very public value.</li>
</ol>
</p>
<h4>Adding the CloudFront distribution to your DNS</h4>
<p>
From the CloudFront dashboard, find your "Domain Name" (which will look like d1XXXXXXX.cloudfront.net). Go to your
DNS provider and add a new <code>CNAME</code> record that maps the domain you chose above (example was cdn.example.com)
to this CloudFront Domain Name.
</p>
<p>
Most DNS UIs will have a form that lets you fill in the <code>value</code>.example.com, where <code>value</code> in our
example would be <code>cdn</code>, such that if you were to put <b>cdn.example.com</b> in the form you would actually
create a record for <code>cdn.example.com.example.com</code>. This mistake would be easy enough to fix but watch out
for that!
</p>

<h4>Create a user to allow Owlkins to write to your S3 bucket</h4>
<p>Now that you have a working S3 + CloudFront configuration, we need to add a "user" that will allow your Owlkins
    distriution to write photos and videos to it.
<ol>
    <li>First, create a policy that will allow your user to access the bucket in the
    <a target="_blank" href="https://console.aws.amazon.com/iamv2/home#/policies">Policies dashboard</a></li>
    <li>Once in the dashboard, click <b>Create Policy</b></li>
    <li>In the new view that appears, select the <b>JSON</b> tab.</li>
    <li>Paste in a policy like the below, where you fill in the name of your bucket:
    {% code_snippet %}
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "s3:PutObject",
                "s3:GetObjectAcl",
                "s3:GetObject",
                "s3:ListBucket",
                "s3:DeleteObject",
                "s3:PutObjectAcl"
            ],
            "Resource": [
                "arn:aws:s3:::YOUR_BUCKET_HERE",
                "arn:aws:s3:::YOUR_BUCKET_HERE/*"
            ]
        }
    ]
}
    {% endcode_snippet %}
    </li>
    <li>Click <b>Next: Tags</b></li>
    <li>You don't need to add any Tags. Click <b>Next: Review</b></li>
    <li>In the review dialog, enter a <b>Name</b> for the policy, perhaps <b>s3-bucket-access-owlkins</b> or something
    that will help you remember the purpose of the policy.</li>
    <li>Click <b>Create policy</b></li>
</ol>
</p>
<p>Now that you have a policy created, you can create the user.
    <ol>
    <li>Navigate to your <a target="_blank" href="https://console.aws.amazon.com/iamv2/home?#/users">AWS IAM dashboard</a></li>
    <li>Click <b>Add users</b></li>
    <li>In the wizard that your window now navigates to, enter a <b>Uesr name</b> that will help you easily
    identify the purpose of this user</li>
    <li>For the <b>Select AWS access type</b> section, Check the box <b>Access key - Programmatic access</b> and leave
    <b>Password - AWS Management Console access</b> unchecked!</li>
    <li>Click <b>Next: permissions</b></li>
    <li>On the permissions screen, select <b>Attach existing policies directly</b></li>
    <li>In the search text field next to <b>Filter policies</b>, type in the <b>Name</b> of the policy you just created.</li>
    <li>Click <b>Next: Tags</b></li>
    <li>You don't have to enter any tags. Click <b>Next: Review</b></li>
    <li>Peruse the user you are about to create and then click <b>Create user</b></li>
    <li>You will now be taken to a screen that shows the user has been created. This screen contains the user's
        credentials <u>which will not be available after you navigate away from this screen.</u> If you navigate away
    from this screen without saving the credentials, either via clicking <b>Download .csv</b> or <b>Show</b> and copying
    the <b>secret access key</b>, then you will have to repeat the above steps.
        <ol>
            <li>Copy the <b>Access key ID</b> and store this in the <code>AWS_ACCESS_KEY_ID</code> variable in your
            <code>secrets.py</code> file.</li>
            <li>Copy the <b>Secret access key</b> and store this in the <code>AWS_SECRET_ACCESS_KEY</code> variable
            in your <code>secrets.py</code> file.</li>
        </ol>
    </li>
    <li>Go back to the <a target="_blank" href="https://console.aws.amazon.com/iamv2/home?#/users">IAM Dashboard</a>
        and you will see your new user. Click on your new user.</li>
    <li>Now in the dashboard for your specific new user, find the <b>User ARN</b> value. Copy this and save it
    as the <code>ARN</code> variable in <code>config.py</code></li>
</ol>

</p>
<h4>All finished with S3 + CloudFront</h4>
<p>That was quite a few steps, but hopefully you've made it through without any issues. Soon we'll find out when we
go to test your Owlkins installation!</p>
{% end_documentation_section %}