from django import template
from django.template.loader import get_template
from django.template.base import Node

register = template.Library()

class CodeSnippetNode(Node):
    def __init__(self, nodelist, exclude_copy=False):
        self.nodelist = nodelist
        self.exclude_copy = exclude_copy

    def render(self, context):
        code_snippet_html = get_template("code_snippet.html")
        return code_snippet_html.render({
            'snippet': self.nodelist.render(context),
            'exclude_copy': self.exclude_copy,
        })

@register.tag('code_snippet')
def code_snippet(parser, token):
    nodelist = parser.parse(('endcode_snippet',))
    tokens = token.split_contents()
    context_extras = [t.split("=")[1].strip('"') for t in tokens[2:]]
    parser.delete_first_token()
    return CodeSnippetNode(nodelist, *context_extras)
