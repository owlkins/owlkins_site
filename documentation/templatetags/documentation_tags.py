from django import template
from django.template.loader import get_template
from django.template.base import Node

register = template.Library()

class DocumentationSectionNode(Node):
    def __init__(self, nodelist, title):
        self.nodelist = nodelist
        self.title = title

    def render(self, context):
        code_snippet_html = get_template("documentation/install/documentation_section.html")
        return code_snippet_html.render({
            'prose': self.nodelist.render(context),
            'title': self.title,
            'number': template.Variable('number').resolve(context) if 'number' in context else '',
        })

@register.tag('documentation_section')
def documentation_section(parser, token):
    nodelist = parser.parse(('end_documentation_section',))
    tokens = token.split_contents()
    context_extras = [t.split("=")[1].strip('"') for t in tokens[2:]]
    parser.delete_first_token()
    return DocumentationSectionNode(nodelist, *context_extras)
