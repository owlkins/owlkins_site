from owlkins_site.urls import return_distill_path

urlpatterns = [
    return_distill_path('privacy'),
    return_distill_path('terms'),
    return_distill_path('license'),
]