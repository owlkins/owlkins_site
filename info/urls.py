from owlkins_site.urls import return_distill_path

urlpatterns = [
    return_distill_path('about'),
    return_distill_path('contribute'),
    return_distill_path('news', other='coming_soon'),
    return_distill_path('blog', other='coming_soon'),
]