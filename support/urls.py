from owlkins_site.urls import return_distill_path

urlpatterns = [
    return_distill_path('pricing'),
    return_distill_path('forum', other='coming_soon'),
    return_distill_path('sample_chrome_password_form'),
]