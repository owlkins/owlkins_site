function copyTextToClipboard(e) {
    function fallbackCopyTextToClipboard(text) {
        var textArea = document.createElement("textarea");
        textArea.value = text;

        // Avoid scrolling to bottom
        textArea.style.top = "0";
        textArea.style.left = "0";
        textArea.style.position = "fixed";

        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();

        try {
            var successful = document.execCommand('copy');
            if (successful) {
                setTextThenReset('Copied!');
            } else {
                setTextThenReset('Error!');
            }
        } catch (err) {
            setTextThenReset('Error!');
        }

        document.body.removeChild(textArea);
    }

    function setTextThenReset(text) {
        e.innerText = '';
        let svg = document.createElementNS('http://www.w3.org/2000/svg',"svg");
        svg.setAttributeNS(null, "class", "h-5 w-5 text-maincolor-600 mx-auto");
        svg.setAttributeNS(null, "fill", "currentColor");
        svg.setAttributeNS(null, "viewBox", "0 0 20 20");
        let path = document.createElementNS('http://www.w3.org/2000/svg',"path");
        path.setAttributeNS(null, "fill-rule", "evenodd");
        path.setAttributeNS(null, "clip-rule", "evenodd");
        path.setAttributeNS(null, "d", "M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z");
        svg.appendChild(path);
        e.appendChild(svg);
        setTimeout(() => {
            e.innerText = 'Copy';
        }, 3000)
    }
    let text = e.parentElement.parentElement.children[0].children[0].innerText;
    if (!navigator.clipboard) {
        fallbackCopyTextToClipboard(text, e);
        return;
    }
    navigator.clipboard.writeText(text).then(function () {
        setTextThenReset('Copied!');
    }, function (err) {
        setTextThenReset('Error!');
    });
}