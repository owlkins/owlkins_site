# owlkins_site

This repository represents the files needed to create the static site 
[https://owlkins.com](https://owlkins.com).

At a high level, the content on the site is composed of several templates
via the [Django template system](https://docs.djangoproject.com/en/4.1/ref/templates/language/).
Styling is via the [Tailwind CSS Framework](https://tailwindcss.com/) with
some components from [Tailwind UI](https://tailwindui.com/).

The actual site files are created using the [Django Distill](https://django-distill.com/) 
generator. 
- In development, you can run the [Django runserver management 
command](https://docs.djangoproject.com/en/4.1/ref/django-admin/#runserver)
in addition to the `tailwind/tailwind_refresh.sh` script to iterate on changes to the site. 
- When the site is ready to be generated, the `distill_export.sh` script 
can be run, and the site structure will be generated. Almost all pages 
are named `index.html` within the appropriate folder structure, allowing 
the site to have "pretty URLs" when uploaded to the web server. 

In the case of [https://owlkins.com](https://owlkins.com), the web server itself
is a "[S3 static site](https://docs.aws.amazon.com/AmazonS3/latest/userguide/WebsiteHosting.html)"
hosted via [CloudFront](https://docs.aws.amazon.com/AmazonS3/latest/userguide/website-hosting-custom-domain-walkthrough.html).

For now the deployment of any generated files to S3 and any resulting
invalidations in CloudFront is manually performed, but in time might be
automated.

